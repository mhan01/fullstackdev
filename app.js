const express = require("express");
const bodyparser = require("body-parser");

let app = express();

//DATABASE
let database = [];
let id = 100;

//USER DATABASE
let registeredUsers = [];
let loggedUsers = [];

//MIDDLEWARE

app.use(bodyparser.json());

createToken = () => {
	let token = ""
	let letters = "abcdefghjiABCDEFGHJI123456789"
	for(let i=0;i<1024;i++) {
		let temp = Math.floor(Math.random()*30);
		token = token + letters[temp]
	}
	return token;
}

isUserLogged = (req,res,next) => {
//	console.log(req.headers.token);
	if(req.headers.token.length>0) {
		let token = req.headers.token;
		for(let i = 0;i<loggedUsers.length;i++) {
			if (loggedUsers[i].token === token) {				
				return next();
			}
		}
	}
	//console.log("wrong token2 = "+token);
	return res.status(403).json({"message":"forbidden"});
}
app.use("/api",isUserLogged);

//LOGIN  API
app.post("/register", function(req,res) {
	if (!req.body)  {
		return res.status(409).json({"message":"provide credentials1"});
	}
	if (!req.body.username || !req.body.password) {
		return res.status(409).json({"message":"provide credentials2"});
	}		
	let user =  {
		username:req.body.username,
		password:req.body.password
	}
	for (let i=0;i<registeredUsers.length;i++) {
		if(user.username === registeredUsers[i].username) {
			return res.status(409).json({"message":"username already in use"});
		}
	}
	registeredUsers.push(user);
	console.log(registeredUsers);
	res.status(200).json({"message":"succes"});
			
});

app.post("/login", function(req,res)  {
	if (!req.body)  {
		return res.status(409).json({"message":"wrong credentials"});
	}
	if (!req.body.username || !req.body.password) {
		return res.status(409).json({"message":"wrong credentials"});
	}		
	let user =  {
		username:req.body.username,
		password:req.body.password
	}	
	for (let i=0;i<registeredUsers.length;i++) {
		if (user.username === registeredUsers[i].username) {
			if (user.password === registeredUsers[i].password) {
				let token = createToken();
				loggedUsers.push({
					"username":user.username,
					"token":token
				})
				console.log(loggedUsers);
				return res.status(200).json({"token":token})
			}
		}
	}
	return res.status(409).json({"message":"wrong credentials"});
});

// REST API

app.get("/api/shopping", function(req,res) {
	res.status(200).json(database);
});

app.post("/api/shopping", function(req,res) {
		let item = {
			type:req.body.type,
			count:req.body.count,
			price:req.body.price,
			id:id
		}
		id++;
		database.push(item);
		res.status(200).json({"message":"succes"});
});

app.delete("/api/shopping/:id", function(req,res) {
	let tempId = parseInt(req.params.id,10);
	for (let i=0;i<database.length;i++) {
		if (tempId === database[i].id) {
			database.splice(i,1);
			return res.status(200).json({"message":"succes"});			
		}
	}
	res.status(404).json({"message":"not found"});			
});

app.put("/api/shopping/:id", function(req,res) {
		let tempId = parseInt(req.params.id,10);
		let item = {
			type:req.body.type,
			count:req.body.count,
			price:req.body.price,
			id:tempId
		}
	for (let i=0;i<database.length;i++) {
		if (tempId === database[i].id) {
			database.splice(i,1,item);
			return res.status(200).json({"message":"succes"});			
		}
	}
	res.status(404).json({"message":"not found"});			
});

app.listen(3001);
console.log("Running in port 3001");
