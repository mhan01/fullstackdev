const mongoose = require("mongoose");

let Schema = mongoose.Schema({
	title:String,
	description:String,
    status:Number,
    AssignmentDay:Date,
    deadline:Date,
	username:{type:String,indexed:true}
});

module.exports = mongoose.model("TicketItem",Schema);