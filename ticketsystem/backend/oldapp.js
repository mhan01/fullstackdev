const express = require("express");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
const userModel = require("./models/user");
const sessionModel = require("./models/session");
const apiRouter = require("./routes/apirouter");

let app = express();

//DATABASE
//let database = [];
//let id = 100;

mongoose.connect("mongodb://localhost/myticketdb").then(
		() => {console.log("Success in connecting mongodb")},
		error => {console.log("Error in connecting Mongodb:"+error)}
);

//MIDDLEWARE

app.use(bodyparser.json());

createToken = () => {
	let token = ""
	let letters = "abcdefghjiABCDEFGHJI123456789"
	for(let i=0;i<1024;i++) {
		let temp = Math.floor(Math.random()*30);
		token = token + letters[temp]
	}
	return token;
}

isUserLogged = (req,res,next) => {
	let token = req.headers.token;
	if(!token) {
		return res.status(403).json({"message":"forbidden"});
	}
	sessionModel.findOne({"token":token} , function(err,session) {
		if (err) {
			return res.status(403).json({"message":"forbidden"});
		}
		if (!session) {
			return res.status(403).json({"message":"forbidden"});
		}
		let now = new Date().getTime();
		if(now > session.ttl) {
			sessionModel.deleteOne({"_id":session.id} , function(err) {
				return res.status(403).json({"message":"forbidden"});
			})
		} else {
//			console.log(session);
			req.session = {};
			req.session.username = session.username;
			session.ttl = now+ttl_diff;
			session.save(function(err,session) {
				return next();
			})
		}
			
	});
}
app.use("/api",isUserLogged, apiRouter);

app.listen(3001);
console.log("Running in port 3001");
