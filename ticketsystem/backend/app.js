const express = require("express");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
const userModel = require("./models/user");
const sessionModel = require("./models/session");
const apiRouter = require("./routes/apirouter");

let app = express();


let ttl_diff = 1000*60*60;
//let ttl_diff = 1000*10;

let port = process.env.PORT || 3001;

mongoose.connect("mongodb://localhost/myticketdb").then(
		() => {console.log("Success in connecting mongodb")},
		error => {console.log("Error in connecting Mongodb:"+error)}
);

//MIDDLEWARE

app.use(bodyparser.json());

createToken = () => {
	let token = ""
	let letters = "abcdefghjiABCDEFGHJI123456789"
	for(let i=0;i<1024;i++) {
		let temp = Math.floor(Math.random()*30);
		token = token + letters[temp]
	}
	return token;
}

isUserLogged = (req,res,next) => {
	let token = req.headers.token;
	if(!token) {
		return res.status(403).json({"message":"forbidden"});
	}
	sessionModel.findOne({"token":token} , function(err,session) {
		if (err) {
			return res.status(403).json({"message":"forbidden"});
		}
		if (!session) {
			return res.status(403).json({"message":"forbidden"});
		}
		let now = new Date().getTime();
		if(now > session.ttl) {
			sessionModel.deleteOne({"_id":session.id} , function(err) {
				return res.status(403).json({"message":"forbidden"});
			})
		} else {
//			console.log(session);
			req.session = {};
			req.session.username = session.username;
			session.ttl = now+ttl_diff;
			session.save(function(err,session) {
				return next();
			})
		}
			
	});
}
app.use("/api",isUserLogged, apiRouter);

//LOGIN  API
app.post("/register", function(req,res) {
	console.log(req.body);
	console.log(" 1 ");
	if (!req.body)  {
		return res.status(422).json({"message":"provide credentials1"});
	}
	if (!req.body.username || !req.body.password) {
		return res.status(422).json({"message":"provide credentials2"});
	}
	if (req.body.username.length < 4 || req.body.password.length < 4) {
		return res.status(422).json({"message":"provide credentials3"});
	}		
	let user = new userModel( {
		username:req.body.username,
		password:req.body.password
	});

	user.save(function(err,user) {
		if (err) {
			console.log("Register failed . Reason: "+err);
			return res.status(422).json({"message":"username already in use"});
		} else
		{
			console.log("Register succes: "+user.username);
			return res.status(200).json({"message":"succes"});
		}			
	});
});

app.post("/login", function(req,res)  {
	console.log(req.body);
	console.log(" 2 ");
	if (!req.body)  {
		return res.status(422).json({"message":"wrong credentials"});
	}
	if (!req.body.username || !req.body.password) {
		return res.status(422).json({"message":"wrong credentials"});
	}
	if (req.body.username.length < 4 || req.body.password.length < 4) {
		return res.status(422).json({"message":"wrong credentials3"});
	}		
	userModel.findOne({"username":req.body.username} , function(err,user) {
		if (err) {
			return res.status(422).json({"message":"wrong credentials"});
		}
		if (!user) {
			return res.status(422).json({"message":"wrong credentials"});
		}		
		//Todo:
		if (user.password === req.body.password) {
			let token = createToken();
			let ttl = new Date().getTime()+ttl_diff;
			let session = new sessionModel ({
				"username":user.username,
				"ttl":ttl,
				"token":token
			})
			session.save(function(err,session) {
				if (err) {
					return res.status(422).json({"message":"wrong credentials"});
				}
				return res.status(200).json({"token":token});
			});
		}	
		else
		{
			return res.status(422).json({"message":"wrong credentials"});
		}
	});

});

app.post("/logout", function(req,res) {
	let token = req.headers.token;
	if(token) {
		sessionModel.findOne({"token":token} , function(err,session) {
			if(err) {
				console.log("Find session failed still logging out:");
			}
			sessionModel.deleteOne({"_id":session.id} , function(err) {
				if(err) {
					console.log("Remove session failed");
				}
			})				
		})
	}
	return res.status(200).json({"message":"success"});
});

app.listen(port);
console.log("Running in port " + port);
