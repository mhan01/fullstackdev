import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from 'react-router-dom';
import {createStore,applyMiddleware,combineReducers} from 'redux';
import loginReducer from './reducers/loginreducer';
import shoppingReducer from './reducers/shoppingreducer';
import ticketReducer from './reducers/ticketreducer';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

ReactDOM.render(
<BrowserRouter>
	<App />
</BrowserRouter>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
