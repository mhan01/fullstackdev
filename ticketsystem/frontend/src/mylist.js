import React from 'react';
import {Table,Button} from 'semantic-ui-react';
import NormalRow from './normalrow';
import DeleteRow from './deleterow';
import EditRow from './editrow';
//import {removeFromList,editFromList} from './actions/shoppingactions';
import {removeFromList,editItem} from './actions/ticketactions';

export default class MyList extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			removeIndex:-1,
			editIndex:-1
		}
	}
	remove = (id) => {
//		 console.log(event.target.name);
//		this.props.removeFromList(id);
//		let tempId = parseInt(id,10);
		for (let i=0;i<this.props.list.length;i++) {
			if (this.props.list[i]._id === id) {
				this.setState({
					removeIndex:i,
					editIndex:-1
				})
			return;
			}
		}
	}

	edit = (id) => {
//		 console.log(event.target.name);
//		this.props.removeFromList(id);
//		let tempId = parseInt(id,10);
		for (let i=0;i<this.props.list.length;i++) {
			if (this.props.list[i]._id === id) {
				this.setState({
					removeIndex:-1,
					editIndex:i
				})
			return;
			}
		}
	}
	
	handleRemove = (id) => {
		this.props.removeFromList(id);
		this.cancel();
	}
	
	editItem = (item) => {
		this.props.editItem(item);
		this.cancel();
	}
	
	cancel = () => {
		this.setState({
			removeIndex:-1,
			editIndex:-1
		})
	}
	
	render() {
		let listitems = this.props.list.map((item,index) => {
			if (this.state.removeIndex === index) {
				return <DeleteRow item={item} 
									key={item._id}
									handleRemove={this.handleRemove}
									cancel={this.cancel}/>
									
			}			
			if (this.state.editIndex === index) {			
				return <EditRow key={item._id}
						item={item}
						editItem={this.editItem}
						cancel={this.cancel}/>
			}
			return <NormalRow key={item._id} 
						removeFromList={this.remove}
						edit={this.edit}
						item={item}/>
		})
		return (
			<Table celled>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Title</Table.HeaderCell>
						<Table.HeaderCell>Description</Table.HeaderCell>
						<Table.HeaderCell>DeadLine</Table.HeaderCell>
						<Table.HeaderCell>Remove</Table.HeaderCell>
						<Table.HeaderCell>Edit</Table.HeaderCell>
					</Table.Row>	
				</Table.Header>
				<Table.Body>
					{listitems}
				</Table.Body>
			</Table>
		)		
	}
	
}