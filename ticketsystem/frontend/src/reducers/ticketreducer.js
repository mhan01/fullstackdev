import {
	GET_TICKETLIST_SUCCESS,
	GET_TICKETLIST_FAILED,
	ADD_TO_LIST_SUCCESS,
	ADD_TO_LIST_FAILED,
	REMOVE_FROM_LIST_SUCCESS,
	REMOVE_FROM_LIST_FAILED,
	EDIT_TICKET_SUCCESS,
	EDIT_TICKET_FAILED,
	LOGOUT_DONE
} from '../actions/ticketactions';


const getInitialSate = () => {
	if (sessionStorage.getItem("ticketstate")) {
		let state = JSON.parse(sessionStorage.getItem("ticketstate"));
		return state;
	}
	else {
		return {
			list:[],
			error:""
		}
	}
}
const saveToStorage = (state) => {
	sessionStorage.setItem("ticketstate",JSON.stringify(state));
}

const initialState = getInitialSate();

const ticketReducer = (state = initialState, action) => {
	let tempState = {};
switch (action.type) {
	case GET_TICKETLIST_SUCCESS:
		tempState = {
			error:"",
			list:action.list
		}
		saveToStorage(tempState);
		return tempState;
	case GET_TICKETLIST_FAILED:
		tempState = {
			...state,
			error:action.error
		}
		saveToStorage(tempState);
		return tempState;	
	case ADD_TO_LIST_SUCCESS:
		tempState = {
			...state,
			error:""
		}
		saveToStorage(tempState);
		return tempState;	
	case ADD_TO_LIST_FAILED:
		tempState = {
			...state,
			error:action.error
		}
		saveToStorage(tempState);
		return tempState;
	case REMOVE_FROM_LIST_SUCCESS:
		tempState = {
			...state,
			error:""
		}
		saveToStorage(tempState);
		return tempState;	
	case REMOVE_FROM_LIST_FAILED:
		tempState = {
			...state,
			error:action.error
		}
		saveToStorage(tempState);
		return tempState;	
	case EDIT_TICKET_SUCCESS:
		tempState = {
			...state,
			error:""
		}
		saveToStorage(tempState);
		return tempState;	
	case EDIT_TICKET_FAILED:
		tempState = {
			...state,
			error:action.error
		}
		saveToStorage(tempState);
		return tempState;	
	case LOGOUT_DONE:
		tempState = {
			list:[],
			error:""
		}
		saveToStorage(tempState);
		return tempState;			
	default:
		return state;
	}
}
