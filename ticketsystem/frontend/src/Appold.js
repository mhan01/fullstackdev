import React from 'react';
//import logo from './logo.svg';
import './App.css';
import MyForm from './myform';
import MyList from './mylist';
import {Switch,Route} from 'react-router-dom';
import NavBar from './navbar';

class App extends React.Component{
	
	constructor(props) {
		super(props);
		this.state = {
			list:[]
		}
	}

	componentDidMount() {
		console.log("Component did mount - App.js");
		this.getShoppingList();
	}
	
	getShoppingList = () => {
		let request = {
			method:"GET",
			mode:"cors",
			headers:{"Content-type":"application/json"}
		}
		fetch("api/shopping",request).then(response => {
			if (response.ok) {
				response.json().then(data => {
					this.setState({list:data});
				}).catch(error => {
					console.log("Error in parsing json");
				})
			}
			else {
				console.log("Server responded with status: " + response.statusText);
			}				
		}).catch(error => {
			console.log(error);
		})
	}
	
	addToList = (item) => {
		let request = {
			method:"POST",
			mode:"cors",
			headers:{"Content-type":"application/json"},
			body:JSON.stringify(item)
		}
		fetch("api/shopping",request).then(response => {
			if (response.ok) {
				console.log("addTolist succes");
				this.getShoppingList();
			}
			else {
				console.log("Server responded with status: " + response.statusText);
			}
		}).catch(error => {
			console.log(error);
		})
	/*	let tempId = this.state.id;
		item.id = this.state.id;
		let templist = this.state.list.concat(item);
		this.setState({
			list:templist,
			id:tempId+1
		}, () => { console.log(this.state)})*/
	}
	
	editItem = (item) => {
/*			let templist = this.state.list.slice(0);
			for (let i=0;i<templist.length;i++) {
				if (item.id === templist[i].id) {
					templist.splice(i,1,item);
					this.setState({
						list:templist
					})
					return;
				}
			}				*/
		let request = {
			method:"PUT",
			mode:"cors",
			headers:{"Content-type":"application/json"},
			body:JSON.stringify(item)
		}
		fetch("api/shopping/"+item.id,request).then(response => {
			if (response.ok) {
				console.log("editItem succes");
				this.getShoppingList();
			}
			else {
				console.log("Server responded with status: " + response.statusText);
			}
		}).catch(error => {
			console.log(error);
		})
			
	}
	
	removeFromList = (id) => {
/*		let templist = [];
		let tempId = parseInt(id,10);
		for (let i=0;i<this.state.list.length;i++) {
			if(tempId !== this.state.list[i].id){
				templist.push(this.state.list[i]);
			}
		}
		this.setState({
			list:templist
		})*/
		
		let request = {
			method:"DELETE",
			mode:"cors",
			headers:{"Content-type":"application/json"},
		}
		fetch("api/shopping/"+id,request).then(response => {
			if (response.ok) {
				console.log("Removelist succes");
				this.getShoppingList();
			}
			else {
				console.log("Server responded with status: " + response.statusText);
			}
		}).catch(error => {
			console.log(error);
		})

	}
	
	render() {
  return (
    <div className="App">
		<NavBar  />
		<hr/>
		<Switch>
			<Route exact path="/" render={() =>
				<MyList list={this.state.list}
				editItem={this.editItem}
				removeFromList={this.removeFromList}/>							
			}/>
			<Route path="/form" render={() =>
				<MyForm addToList={this.addToList}/>
			}/>			
		</Switch>			
    </div>
  );
}
}
export default App;

