import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyForm from './myform';
import MyList from './mylist';
import {Switch,Route,Redirect,withRouter} from 'react-router-dom';
import LoginForm from './loginform';
import Navbar from './navbar';

class App extends React.Component{
  
  constructor(props) {
	  super(props);
	  this.state= {
			list:[],
			isLogged:false,
			token:""
	  }
  }
  
  componentDidMount() {
	  console.log("Component Did Mount - App.js");
	  if(sessionStorage.getItem("state")) {
		  console.log("state found!");
		  let tempState = JSON.parse(sessionStorage.getItem("state"));
		  this.setState(tempState, () => { if (this.state.isLogged) {this.getShoppingList()}});
	  }
  }
  saveToStorage = () => {
	  sessionStorage.setItem("state", JSON.stringify(this.state));
  }

  handleStatus = (status) => {
	  if(status === 403) {
		  this.setState({
			token:"",
			isLogged:false
		  }, () => {this.saveToStorage()});
	  }
  }

  login = (user) => {
	  let request = {
		  method:"POST",
		  mode:"cors",
		  headers:{"Content-Type":"application/json"},
		  body:JSON.stringify(user)
	  }
	  fetch("/login", request).then(response => {
		  if(response.ok) {
			  response.json().then(data => {
				  this.setState({
					  isLogged:true,
					  token:data.token
				  }, () => {
					  this.getShoppingList();
					  this.saveToStorage();
				  })
			  }).catch(error => {
				  console.log("Error parsing JSON");
			  })
		  } else {
			  console.log("Server responded with status:"+response.status);
		  }
	  }).catch(error => {
		  console.log(error);
	  })
  }

  //login api
  /*login = (user) => {
    console.log("login");
  }
*/
  /*render() {
  return (
    <div className="App">
				<Navbar/>
			<hr/>
      <Route exact path="/" render={() => 
					(<LoginForm register={this.register}
							   login={this.login}/>)	
				}/>
    </div>
  )};*/

//  <LoginForm login={this.login} />			
render() {
  return (
  <div className="App">
    <Navbar isLogged={this.state.isLogged}
      logout={this.logout}/>
    <hr/>
    <Switch>
      <Route exact path="/" render={() => 
        this.state.isLogged ?
        (<Redirect to="/list"/>) :
        (<LoginForm register={this.register}
               login={this.login}/>)	
      }/>
      <Route path="/list" render={() => 
        this.state.isLogged ?				
        (<MyList list={this.state.list}
            removeFromList={this.removeFromList}
            editItem={this.editItem}/>) :
        (<Redirect to="/"/>)
      }/>
      <Route path="/form" render={() => 
        this.state.isLogged ?
        (<MyForm addToList={this.addToList}/>) :
        (<Redirect to="/"/>)
      }/>
      <Route render={() => 
        (<Redirect to="/"/>)
      }/>
    </Switch>
  </div>
  );
}
}

/*function App() {
  return (
    <div className="App">
				<Navbar/>
			<hr/>
			<LoginForm />			
    </div>
  );
}
*/
export default withRouter(App);
