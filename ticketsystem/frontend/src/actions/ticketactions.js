import {fetchLoading,loadingDone} from './loginactions';
//ticket actions
export const GET_TICKETLIST_SUCCESS = "GET_SHOPPINGLIST_SUCCESS";
export const GET_TICKETLIST_FAILED = "GET_TICKETLIST_FAILED";
export const ADD_TO_LIST_SUCCESS = "ADD_TO_LIST_SUCCESS";
export const ADD_TO_LIST_FAILED = "ADD_TO_LIST_FAILED";
export const REMOVE_FROM_LIST_SUCCESS = "REMOVE_FROM_LIST_SUCCESS";
export const REMOVE_FROM_LIST_FAILED = "REMOVE_FROM_LIST_FAILED";
export const EDIT_TICKET_SUCCESS = "EDIT_TICKET_SUCCESS";
export const EDIT_TICKET_FAILED = "EDIT_TICKET_FAILED";
export const LOGOUT_DONE = "LOGOUT_DONE";

export const getTicketList = (token) => {
	return dispatch => {
	  let request = {
		  method:"GET",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token}
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/tickets",request).then(response => {
			dispatch(loadingDone());
			if(response.ok) {
				response.json().then(data => {
						dispatch(getTicketListSuccess(data));
					}
				).catch(error => {
	//				console.log("Error in parsing response json");
					dispatch(getTicketListFailed("Error in parsing response json"))
				});
			} else {
	//			console.log("Server responded with status:"+response.statusText);
				dispatch(getTicketListFailed("Server responded with status:"+response.statusText))

			}}).catch(error => {
			dispatch(loadingDone());
		//	console.log(error);
			dispatch(getTicketListFailed(error));
		});
}}

export const addTolist = (item,token,history) =>  {
	return dispatch => {
	  let request = {
		  method:"POST",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token},
		  body:JSON.stringify(item)
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/tickets",request).then(response => {
			if(response.ok) {
//				console.log("addToList success");
				dispatch(addToListSuccess());
				dispatch(getTicketList(token));
				history.push("/list");
			} else {
				dispatch(addToListFailed("Server responded with status:"+response.statusText));
				dispatch(loadingDone());
			}
	  }).catch(error => {
				dispatch(addToListFailed(error));
				dispatch(loadingDone());
	  })		
	}
}

export const removeFromList = (id,token) => {
	return dispatch => {			
		let request = {
		  method:"DELETE",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token}
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/tickets/"+id,request).then(response => {
			if(response.ok) {
				dispatch(removeFromListSuccess(token));
				dispatch(getTicketList(token));
//				this.getShoppingList();
			} else {
				dispatch(removeFromListFailed("Server responded with status: "+response.statusText));
				dispatch(loadingDone());
//				console.log("Server responded with status:"+response.statusText);
			}
	  }).catch(error => {
//			console.log(error);
			dispatch(removeFromListFailed(error));
			dispatch(loadingDone());
	  })
	}
}

export const editItem = (item,token) => {
	return dispatch => {			
	  let request = {
		  method:"PUT",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token},
		  body:JSON.stringify(item)
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/tickets/"+item._id,request).then(response => {
			if(response.ok) {
				dispatch(editTicketSuccess);
				dispatch(getTicketList(token));
//			this.getShoppingList();
			} else {
				//console.log("Server responded with status:"+response.statusText);
				dispatch(loadingDone());
				dispatch(editTicketFailed("Server responded with status: "+response.statusText));
			}
	  }).catch(error => {
			dispatch(loadingDone());
			dispatch(editTicketFailed(error));
			
//			console.log(error);
	  })
	}	
}

//actions creators


const getTicketListSuccess = (list) => {
	return {
		type:GET_TICKETLIST_SUCCESS,
		list:list
	}
	
}

const getTicketListFailed = (error) => {
	return {
		type:GET_TICKETLIST_FAILED,
		error:error
	}
	
}

const addToListSuccess = () => {
	return {
		type:ADD_TO_LIST_SUCCESS
	}
}
	
const addToListFailed = (error) => {
	return {
		type:ADD_TO_LIST_FAILED,
		error:error
	}
	
}

const removeFromListSuccess = () => {
	return {
		type:REMOVE_FROM_LIST_SUCCESS
	}
}

const removeFromListFailed = (error) => {
	return {
		type:REMOVE_FROM_LIST_FAILED,
		error:error
	}
}

const editTicketSuccess = () => {
	return {
		type:EDIT_TICKET_SUCCESS
	}
}
	
const editTicketFailed = (error) => {
	return {
		type:EDIT_TICKET_FAILED,
		error:error
	}
	
}

export const logoutDone = () => {
	return {
		type:LOGOUT_DONE
	}
}