import {fetchLoading,loadingDone} from './loginactions';

//shopping actions
export const GET_SHOPPINGLIST_SUCCESS = "GET_SHOPPINGLIST_SUCCESS";
export const GET_SHOPPINGLIST_FAILED = "GET_SHOPPINGLIST_FAILED";
export const ADD_TO_LIST_SUCCESS = "ADD_TO_LIST_SUCCESS";
export const ADD_TO_LIST_FAILED = "ADD_TO_LIST_FAILED";
export const REMOVE_FROM_LIST_SUCCESS = "REMOVE_FROM_LIST_SUCCESS";
export const REMOVE_FROM_LIST_FAILED = "REMOVE_FROM_LIST_FAILED";
export const EDIT_ITEM_SUCCESS = "EDIT_ITEM_SUCCESS";
export const EDIT_ITEM_FAILED = "EDIT_ITEM_FAILED";
export const LOGOUT_DONE = "LOGOUT_DONE";

//actions

export const getShoppingList = (token) => {
	return dispatch => {
	  let request = {
		  method:"GET",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token}
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/shopping",request).then(response => {
			dispatch(loadingDone());
			if(response.ok) {
				response.json().then(data => {
						dispatch(getShoppingListSuccess(data));
					}
				).catch(error => {
	//				console.log("Error in parsing response json");
					dispatch(getShoppingListFailed("Error in parsing response json"))
				});
			} else {
	//			console.log("Server responded with status:"+response.statusText);
				dispatch(getShoppingListFailed("Server responded with status:"+response.statusText))

			}}).catch(error => {
			dispatch(loadingDone());
		//	console.log(error);
			dispatch(getShoppingListFailed(error));
		});
}}

export const addTolist = (item,token,history) =>  {
	return dispatch => {
	  let request = {
		  method:"POST",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token},
		  body:JSON.stringify(item)
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/shopping",request).then(response => {
			if(response.ok) {
//				console.log("addToList success");
				dispatch(addToListSuccess());
				dispatch(getShoppingList(token));
				history.push("/list");
			} else {
				dispatch(addToListFailed("Server responded with status:"+response.statusText));
				dispatch(loadingDone());
			}
	  }).catch(error => {
				dispatch(addToListFailed(error));
				dispatch(loadingDone());
	  })		
	}
}

export const removeFromList = (id,token) => {
	return dispatch => {			
		let request = {
		  method:"DELETE",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token}
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/shopping/"+id,request).then(response => {
			if(response.ok) {
				dispatch(removeFromListSuccess(token));
				dispatch(getShoppingList(token));
//				this.getShoppingList();
			} else {
				dispatch(removeFromListFailed("Server responded with status: "+response.statusText));
				dispatch(loadingDone());
//				console.log("Server responded with status:"+response.statusText);
			}
	  }).catch(error => {
//			console.log(error);
			dispatch(removeFromListFailed(error));
			dispatch(loadingDone());
	  })
	}
}

export const editFromList = (item,token) => {
	return dispatch => {			
	  let request = {
		  method:"PUT",
		  mode:"cors",
		  cretentials:"include",
		  headers:{"Content-Type":"application/json",
					"token":token},
		  body:JSON.stringify(item)
	  }
	  dispatch(fetchLoading());
	  return fetch("/api/shopping/"+item._id,request).then(response => {
			if(response.ok) {
				dispatch(editItemSuccess);
				dispatch(getShoppingList(token));
//			this.getShoppingList();
			} else {
				//console.log("Server responded with status:"+response.statusText);
				dispatch(loadingDone());
				dispatch(editItemFailed("Server responded with status: "+response.statusText));
			}
	  }).catch(error => {
			dispatch(loadingDone());
			dispatch(editItemFailed(error));
			
//			console.log(error);
	  })
	}	
}

//actions creattors

const getShoppingListSuccess = (list) => {
	return {
		type:GET_SHOPPINGLIST_SUCCESS,
		list:list
	}
	
}

const getShoppingListFailed = (error) => {
	return {
		type:GET_SHOPPINGLIST_FAILED,
		error:error
	}
	
}

const addToListSuccess = () => {
	return {
		type:ADD_TO_LIST_SUCCESS
	}
}
	
const addToListFailed = (error) => {
	return {
		type:ADD_TO_LIST_FAILED,
		error:error
	}
	
}

const removeFromListSuccess = () => {
	return {
		type:REMOVE_FROM_LIST_SUCCESS
	}
}

const removeFromListFailed = (error) => {
	return {
		type:REMOVE_FROM_LIST_FAILED,
		error:error
	}
}

const editItemSuccess = () => {
	return {
		type:EDIT_ITEM_SUCCESS
	}
}
	
const editItemFailed = (error) => {
	return {
		type:EDIT_ITEM_FAILED,
		error:error
	}
	
}

export const logoutDone = () => {
	return {
		type:LOGOUT_DONE
	}
}